﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VidlyNew.Dto;
using VidlyNew.Models;

namespace VidlyNew.Controllers.API
{
    public class CustomerController : ApiController
    {
        private ApplicationDbContext _context;
        public CustomerController()
        {
            _context = new ApplicationDbContext();
        }
        //Get /api/Custommer (all)
        public IEnumerable<CustomerDto> GetCustomers() {
            return _context.Customers.ToList().Select(Mapper.Map<Customer,CustomerDto>);

        }

        //Get /api/Customer/Id(integer)
        public IHttpActionResult GetCustomer(int id) {

            var cus = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (cus == null) {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return Ok(Mapper.Map<Customer,CustomerDto>(cus));    
        }

        //Post /api/customer
        [HttpPost]
        public IHttpActionResult CreateCustomer(CustomerDto customerDto, int Id  ) {

            //validate Exception
            if (!ModelState.IsValid) {
                return BadRequest();
            }
            //   var cusDb = _context.Customers.SingleOrDefault(c => c.Id == Id);
            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);
            _context.Customers.Add(customer);
            _context.SaveChanges();
            customerDto.Id = customer.Id;
            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customerDto);

        }
        //put /api
        [HttpPut]
        public void UpdateCustomer(CustomerDto customerDto, int Id)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var cusDb = _context.Customers.SingleOrDefault(c => c.Id == Id);
            if (cusDb == null) {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }

            Mapper.Map<CustomerDto, Customer>(customerDto, cusDb);
            /*     cusDb.Name = customer.Name;
            cusDb.BirthDate = customer.BirthDate;
            cusDb.MembershipTypeId = customer.MembershipTypeId;
            cusDb.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;*/
            _context.SaveChanges();
        }

        //Delete /api/customer/1
        [HttpDelete]
        public void DeleteCustomer(int id) {
            var cusdb = _context.Customers.SingleOrDefault(c => c.Id == id);
            if (cusdb == null) {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            _context.Customers.Remove(cusdb);
            _context.SaveChanges();

        }
    }
}
