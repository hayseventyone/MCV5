﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VidlyNew.Models;
using System.Data.Entity;
using VidlyNew.ViewModels;

namespace VidlyNew.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        // GET: Customer

        private ApplicationDbContext _context;

        public CustomersController() {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

         public ActionResult Index()
           {
             var customers = _context.Customers.Include(cus => cus.MembershipType).ToList();

            return View(customers);
           }

          public ActionResult Details(int Id)
           {
             var customer = _context.Customers.Include(cus => cus.MembershipType).SingleOrDefault(c => c.Id == Id);
               if (customer == null)
                   return HttpNotFound();

               return View(customer);

           }

        public ActionResult New() {
            var membershipTypes = _context.MembershipTypes.ToList();
            var viewModel = new CustomerFormViewModel
            {
                Customer = new Customer(),
                MembershipTypes = membershipTypes
            };
            return View("CustomerForm",viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer ) {

            if (!ModelState.IsValid) {
                var viewModel = new CustomerFormViewModel
                {
                    Customer = customer,
                    MembershipTypes = _context.MembershipTypes.ToList()

                };
                return View("CustomerForm",viewModel);
            }

            if (customer.Id == 0)
                _context.Customers.Add(customer);
            else
            {
                var CustomerInDb = _context.Customers.Single(c => c.Id == customer.Id);

                //  TryUpdateModel(CustomerInDb,"", new string[] { "Name","Email"});
                CustomerInDb.Name = customer.Name;
                CustomerInDb.BirthDate = customer.BirthDate;
                CustomerInDb.MembershipTypeId = customer.MembershipTypeId;
                CustomerInDb.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;

            }

            _context.SaveChanges();

            return RedirectToAction("Index","Customers");
        }

        public ActionResult Edit(int id) {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);
            if (customer == null)
                return HttpNotFound();
            var viewModel = new CustomerFormViewModel
            {
                Customer = customer,
                MembershipTypes = _context.MembershipTypes.ToList()

            };
            return View("CustomerForm", viewModel);
        }



    }
}