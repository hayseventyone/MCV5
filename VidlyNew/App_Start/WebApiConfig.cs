﻿using Newtonsoft.Json.Serialization;
using System.Web.Http;
using System.Xml;
using Newtonsoft.Json;
namespace VidlyNew
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var setCamel = config.Formatters.JsonFormatter.SerializerSettings;
            setCamel.ContractResolver = new CamelCasePropertyNamesContractResolver();
            setCamel.Formatting = Newtonsoft.Json.Formatting.Indented;
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
