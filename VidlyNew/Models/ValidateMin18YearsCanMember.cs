﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VidlyNew.Models
{
    public class ValidateMin18YearsCanMember :ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = (Customer)validationContext.ObjectInstance;

            if (customer.MembershipTypeId == MembershipType.Unknow || customer.MembershipTypeId== MembershipType.FreeMember) return ValidationResult.Success;

            if (customer.BirthDate == null) return new ValidationResult("Birth is required");

            var age = DateTime.Today.Year - customer.BirthDate.Value.Year;

            return (age >= 18) 
                ? ValidationResult.Success

                : new ValidationResult("Customer Should be at least 18 Years old to a Member ");
        }
    }
}