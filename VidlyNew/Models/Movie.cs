﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VidlyNew.Models
{
    public class Movie
    {
        public int? Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        public Genre Genre { get; set; }

        public byte GenreId { get; set; }

        [Display(Name ="ReleaseDate")]
        [Required]

        public DateTime DateAdded { get; set; }
        [Range(1,100)]
        [Required]
        public byte NumberInStock { get; set; }
    }
}