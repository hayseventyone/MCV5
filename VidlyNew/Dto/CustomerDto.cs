﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VidlyNew.Models;

namespace VidlyNew.Dto
{
    public class CustomerDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Customer Name can not empty")]
        [StringLength(255)]
        public string Name { get; set; }

        public bool IsSubscribedToNewsletter { get; set; }
    
        public byte MembershipTypeId { get; set; }
  
     //   [ValidateMin18YearsCanMember]
        public DateTime? BirthDate { get; set; }
    }
}