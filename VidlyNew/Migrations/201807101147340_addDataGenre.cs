namespace VidlyNew.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDataGenre : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Genres (Id , Name) VALUES (1,'Action') ");
            Sql("INSERT INTO Genres (Id , Name) VALUES (2,'Commedy') ");
            Sql("INSERT INTO Genres (Id , Name) VALUES (3,'Drama') ");
        }
        
        public override void Down()
        {
        }
    }
}
