namespace VidlyNew.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddValuestoMemberType : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO MembershipTypes (Id , Name, SignupFee, DurationInMonths, DiscountRate) VALUES (1,'Non Member',0,0,0) ");
            Sql("INSERT INTO MembershipTypes (Id , Name, SignupFee, DurationInMonths, DiscountRate) VALUES (2,'Silve Member',40,1,5) ");       
            Sql("INSERT INTO MembershipTypes (Id , Name, SignupFee, DurationInMonths, DiscountRate) VALUES (3,'Gold Member',90,3,10) ");
            Sql("INSERT INTO MembershipTypes (Id , Name, SignupFee, DurationInMonths, DiscountRate) VALUES (4,'Platinum Member',300,12,15) ");
        }
        
        public override void Down()
        {
        }
    }
}
